package fr.uvsq.chapitre6exo1;

public enum zero_un {
	ZERO (0, 1),
	UN (1, 1);
	
	public int n;
	public int d;
	
	zero_un(int a, int b){
		n = a;
		d = b;
	}

}
